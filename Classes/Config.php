<?php
class Config {

    const APIKEY    = 'APIKEY';
    const APITOKEN  = 'APITOKEN';
    const BASEURL   = 'BASEURL';
    const BRAND     = 'BRAND';

}